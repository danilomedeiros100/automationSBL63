package test;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.junit.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.winium.DesktopOptions;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.gargoylesoftware.htmlunit.javascript.host.URL;

import winium.elements.desktop.WiniumDriver;



public class TestBase {
	WiniumDriver driver = null;
	Screen screen = new Screen();

//	@Before
//	public void setUpEnvironment() throws IOException {
//		DesktopOptions options = new DesktopOptions();
//		options.setApplicationPath("C:\\Users\\Accenture\\Desktop\\TI8.bat");
//
//		try {
//			driver = new WiniumDriver(new URL("http://localhost:9999"), options);
//		} catch (MalformedURLException e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//
//	}

// BANCO DE IMAGENS

	Pattern user = new Pattern("C:\\imagens\\sbl63\\user.png");
	Pattern senha = new Pattern("C:\\imagens\\sbl63\\senha.png");
	Pattern entrar = new Pattern("C:\\imagens\\sbl63\\entrar.png");
	Pattern contatos = new Pattern("C:\\imagens\\sbl63\\contatos.png");
	Pattern contatos_ativacao = new Pattern("C:\\imagens\\sbl63\\contatos_ativacao.png");
	Pattern pesquisar_cpf = new Pattern("C:\\imagens\\sbl63\\pesquisar_cpf.png");
	Pattern incluir_nome = new Pattern("C:\\imagens\\sbl63\\incluir_nome.png");
	Pattern togo_email = new Pattern("C:\\imagens\\sbl63\\togo_email.png");
	Pattern campo_email = new Pattern("C:\\imagens\\sbl63\\campo_email.png");
	Pattern campo_pdv = new Pattern("C:\\imagens\\sbl63\\campo_pdv.png");
	Pattern incluir_cep = new Pattern("C:\\imagens\\sbl63\\incluir_cep.png");
	Pattern novo_cep = new Pattern("C:\\imagens\\sbl63\\novo_cep.png");
	Pattern fechar_cep = new Pattern("C:\\imagens\\sbl63\\fechar_cep.png");
	Pattern conta_fatura = new Pattern("C:\\imagens\\sbl63\\conta_fatura.png");
	Pattern perfil_fatura = new Pattern("C:\\imagens\\sbl63\\perfil_fatura.png");
	Pattern analise_credito = new Pattern("C:\\imagens\\sbl63\\analise_credito.png");
	Pattern nova_analise = new Pattern("C:\\imagens\\sbl63\\nova_analise.png");
	Pattern analise_2 = new Pattern("C:\\imagens\\sbl63\\analise_2.png");
	Pattern analise_1 = new Pattern("C:\\imagens\\sbl63\\analise_1.png");
	Pattern revisar_fatura = new Pattern("C:\\imagens\\sbl63\\revisar_fatura.png");
	Pattern contas = new Pattern("C:\\imagens\\sbl63\\contas.png");
	Pattern seta_menu = new Pattern("C:\\imagens\\sbl63\\seta_menu.png");
	Pattern oi_familia = new Pattern("C:\\imagens\\sbl63\\oi_familia.png");
	Pattern cpf_contato = new Pattern("C:\\imagens\\sbl63\\cpf_contato.png");
	Pattern configurar_oi_familia = new Pattern("C:\\imagens\\sbl63\\configurar_oi_familia.png");
	Pattern contato_pdv = new Pattern("C:\\imagens\\sbl63\\contato_pdv.png");
	Pattern contato_plano = new Pattern("C:\\imagens\\sbl63\\contato_plano.png");
	Pattern sbl_barra_tarefa = new Pattern("C:\\imagens\\sbl63\\sbl_barra_tarefa.png");
	
	//----------------------------------------
	
	
	// rola a página da web para cima
	public void scrollUp() {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-500)", "");

	}

	// rola a página da web para baixo
	public void scrollDown() {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	}

	public void report(String text) {
		try {
			PrintWriter writer = new PrintWriter(new FileOutputStream(new File("report.txt"), true));
			writer.println("\n");
			writer.println("--------------------");

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			String date = calendar.get(Calendar.DATE) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/"
					+ calendar.get(Calendar.YEAR);
			String hour = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE);
			writer.print(date + " " + hour);
			writer.println();

			writer.println(text);
			writer.close();
		} catch (IOException e) {
		}
	}

//	public void testeSucesso(Object obj) {
//		text.append(obj.getClass().getSimpleName() + ": SUCESSO!!!");
//	}
//
//	public void testeFalhou(Object obj, Exception e) throws Exception {
//		text.append(obj.getClass().getSimpleName() + ": FALHOU!!!");
//		// text.append(obj.getClass().getSimpleName() + ": Falhou!!!\nErro: " +
//		// e.getMessage() + ")");
//		throw new Exception(e);
//	}

	// Assert.assertTrue("Verificação ",
	// AreaAlunoPage.assert_area_aluno(driver).getText()
	// .equals("Minhas mentorias"));
	// System.out.println("Logado na area do aluno com sucesso");

	// public void LoginTeste() {
	// System.out.println("Módulo de Acesso - teste do método ==>
	// startLoginDropDow...");
	// HomePage.btn_login_cadastro(driver).click();
	// HomePage.email_login(driver).sendKeys(Entradas.ALUNO_TESTE);
	// HomePage.password_login(driver).sendKeys(Entradas.SENHA_ALUNO_TESTE);
	// HomePage.btn_entrar_login(driver).click();
	// }
	//
	// public void LoginAcessoLivre() {
	// System.out.println("Módulo de Acesso - teste do método ==>
	// startLoginDropDow...");
	// HomePage.btn_login_cadastro(driver).click();
	// HomePage.email_login(driver).sendKeys(Entradas.USUARIO_GERENCIADOR);
	// HomePage.password_login(driver).sendKeys(Entradas.SENHA_USUARIO_GERENCIADOR);
	// HomePage.btn_entrar_login(driver).click();
	// }

//	public void AbrirGmail() {
//
//		System.setProperty("webdriver.chrome.driver", "/chromedriver");
//		driver = new ChromeDriver();
//		ChromeOptions options = new ChromeOptions();
//		driver.get("http://www.gmail.com");
//
//		Dimension targetSize = new Dimension(1900, 1300);
//		driver.manage().window().setSize(targetSize);
//		
//		//driver.manage().window().maximize();
//		GmailPage.email(driver).sendKeys(br.com.cers.mentoring.utils.Entrada.ALUNO);
//		GmailPage.confir_email(driver).click();
//		esperar();
//		GmailPage.senha(driver).sendKeys("cupim444");
//		GmailPage.confir_senha(driver).click();
//		esperar();
//	}
//
//	public void loginAluno() {
//
//		HomePage.email_login(driver).sendKeys(br.com.cers.mentoring.utils.Entrada.ALUNO);
//		HomePage.senha_login(driver).sendKeys(br.com.cers.mentoring.utils.Entrada.SENHA_ALUNO);
//		HomePage.btn_entar_login(driver).click();
//
//	}

	public void esperar2() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void esperar5() {

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void esperar05() {
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void esperar30() {
		
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void esperar20() {
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void esperar10() {
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void esperar15() {
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 01
	private static String calcDigVerif(String num) {
		Integer primDig, segDig;
		int soma = 0, peso = 10;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

		if (soma % 11 == 0 | soma % 11 == 1)
			primDig = new Integer(0);
		else
			primDig = new Integer(11 - (soma % 11));

		soma = 0;
		peso = 11;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

		soma += primDig.intValue() * 2;
		if (soma % 11 == 0 | soma % 11 == 1)
			segDig = new Integer(0);
		else
			segDig = new Integer(11 - (soma % 11));

		return primDig.toString() + segDig.toString();
	}

	// 02
	public static String geraCPF() {
		String iniciais = "";
		Integer numero;
		for (int i = 0; i < 9; i++) {
			numero = new Integer((int) (Math.random() * 10));
			iniciais += numero.toString();
		}
		return iniciais + calcDigVerif(iniciais);
	}

	// 03
	public static boolean validaCPF(String cpf) {
		if (cpf.length() != 11)
			return false;

		String numDig = cpf.substring(0, 9);
		return calcDigVerif(numDig).equals(cpf.substring(9, 11));
	}

	/**
	 * Gerar números aleatÛrios
	 */
	public int num_randon() {

		Random gerador = new Random();
		int num = gerador.nextInt(10) + 10;
		return num;
	}

	// Abrir chrome em modo anonimo
	protected CharSequence getNewIncognitoWindowCommand() {
		return Keys.chord(Keys.CONTROL, Keys.SHIFT, "p");
	}

	// troca de telas

	// String janela = driver.getWindowHandle();
	// System.out.println(janela);
	//
	// String janela1 = driver.getWindowHandle();
	// System.out.println(janela1);
	//
	// driver.switchTo().activeElement();
	// driver.switchTo().frame();

	// System.out.println("Teste da janela principal ==>> " + janela);
	// Set<String> janelas = driver.getWindowHandles();

	// if (janelas != null) {
	// for (String s : janelas) {
	// System.out.println("Teste da janela (lista) ==>> " + s);
	// driver.switchTo().window(s);
	// driver.switchTo().frame(s);
	// }
	// janela = driver.getWindowHandle();
	// System.out.println("Teste da janela principal 2 ==>> " + janela);

	// }

	// System.out.println("Teste da janela atual ==>> " + janela);

}
