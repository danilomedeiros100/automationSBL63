package test;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class abrirPlanilha {
	  public static void main(String[] args) throws Exception {

	        FileInputStream fisPlanilha = null;

	        
	            File file = new File("planilhas\\planilhaDaAula.xlsx");
	            fisPlanilha = new FileInputStream(file);

	            //cria um workbook = planilha toda com todas as abas
	            XSSFWorkbook workbook = new XSSFWorkbook(fisPlanilha);

	            //recuperamos apenas a primeira aba ou primeira planilha
	            XSSFSheet sheet = workbook.getSheetAt(0);

	            //retorna todas as linhas da planilha 0 (aba 1)
	            Iterator<Row> rowIterator = sheet.iterator();

	            //varre todas as linhas da planilha 0
	            while (rowIterator.hasNext()) {

	                //recebe cada linha da planilha
	                Row row = rowIterator.next();

	                //pegamos todas as celulas desta linha
	                Iterator<Cell> cellIterator = row.iterator();

	                //varremos todas as celulas da linha atual
	                while (cellIterator.hasNext()) {

	                    //criamos uma celula
	                    Cell cell = cellIterator.next();

	                    switch (cell.getCellType()) {

	                        case Cell.CELL_TYPE_STRING:
	                            System.out.println("TIPO STRING: " + cell.getStringCellValue());
	                            break;

	                        case Cell.CELL_TYPE_NUMERIC:
	                            System.out.println("TIPO NUMERICO: " + cell.getNumericCellValue());
	                            break;
	                            
	                        case Cell.CELL_TYPE_FORMULA:
	                            System.out.println("TIPO FORMULA: " + cell.getCellFormula());
	                    }

	                }
	            }

	       

	    }
}
